package com.bookstore.service.impl;

import com.bookstore.domain.*;
import com.bookstore.repository.CartItemRepository;
import com.bookstore.repository.ProductToCartItemRepository;
import com.bookstore.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
@Service
public class CartItemServiceImpl implements CartItemService {
    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ProductToCartItemRepository productToCartItemRepository;

    @Override
    public List<CartItem> findByShoppingCart(ShoppingCart shoppingCart) {

        return cartItemRepository.findByShoppingCart(shoppingCart);
    }

    @Override
    public CartItem updateCartItem(CartItem cartItem) {
        BigDecimal bigDecimal = new BigDecimal(cartItem.getProduct().getPrice()).multiply(new BigDecimal(cartItem.getQty()));

        bigDecimal = bigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP);
        cartItem.setSubtoltal(bigDecimal);

        cartItemRepository.save(cartItem);

        return cartItem;
    }



    @Override
    public CartItem addProductToCartItem(Product product, String color, String size, User user, int qty) {
        List<CartItem> cartItemList = findByShoppingCart(user.getShoppingCart());

        for(CartItem cartItem : cartItemList){
            if(product.getId() == cartItem.getProduct().getId() && color.equals(cartItem.getColor()) && size.equals(cartItem.getSize())){
                cartItem.setQty(cartItem.getQty() + qty);
                cartItem.setSubtoltal(new BigDecimal(product.getPrice()).multiply(new BigDecimal(qty)));
                cartItemRepository.save(cartItem);
                return cartItem;
            }
        }

        CartItem cartItem = new CartItem();
        cartItem.setShoppingCart(user.getShoppingCart());
        cartItem.setProduct(product);
        cartItem.setQty(qty);
        cartItem.setColor(color);
        cartItem.setSize(size);

        cartItem.setSubtoltal(new BigDecimal(product.getPrice()).multiply(new BigDecimal(qty)));

        cartItem = cartItemRepository.save(cartItem);
        ProductToCartItem productToCartItem = new ProductToCartItem();
        productToCartItem.setProduct(product);
        productToCartItem.setCartItem(cartItem);

        productToCartItemRepository.save(productToCartItem);

        return cartItem;
    }

    @Override
    public CartItem findById(Long id) {
        return cartItemRepository.findOne(id);
    }

    @Override
    public void removeCartItem(CartItem cartItem) {
        productToCartItemRepository.deleteByCartItem(cartItem);
        cartItemRepository.delete(cartItem);
    }

    @Override
    public CartItem save(CartItem cartItem) {
        return cartItemRepository.save(cartItem);
    }
}
