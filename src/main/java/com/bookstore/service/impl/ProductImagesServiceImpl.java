package com.bookstore.service.impl;

import com.bookstore.domain.ProductImages;
import com.bookstore.repository.ProductImagesRepository;
import com.bookstore.service.ProductImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
@Service
public class ProductImagesServiceImpl implements ProductImagesService {
    @Autowired
    private ProductImagesRepository productImagesRepository;
    @Override
    public List<ProductImages> getAll() {
        return (List<ProductImages>) productImagesRepository.findAll();
    }

    @Override
    public ProductImages findById(Long id) {
        return productImagesRepository.findOne(id);
    }
}
