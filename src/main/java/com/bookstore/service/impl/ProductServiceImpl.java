package com.bookstore.service.impl;

import com.bookstore.domain.Product;
import com.bookstore.repository.ProductRepository;
import com.bookstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
@Service
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        return (List<Product>) productRepository.findAll();
    }

    @Override
    public Product findOne(Long id) {
        return productRepository.findOne(id);
    }

    @Override
    public List<Product> findByStatus(String status) {
        return productRepository.findByStatus(status);
    }

    @Override
    public List<Product> findByGender(String gender) {
        return productRepository.findByGender(gender);
    }

    @Override
    public List<Product> findByGenderAndStatus(String gender, String status) {
        return productRepository.findByGenderAndStatus(gender,status);
    }

    @Override
    public List<Product> findBySubCategory(String subCategory) {
        return productRepository.findBySubCategory(subCategory);
    }

    @Override
    public List<Product> searchProduct(String keyword) {
        List<Product> productList = productRepository.findByNameContaining(keyword);

        return productList;
    }

}
