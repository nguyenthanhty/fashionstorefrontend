package com.bookstore.service.impl;
import com.bookstore.domain.Product;
import com.bookstore.domain.Review;
import com.bookstore.domain.User;
import com.bookstore.repository.ReviewRepository;
import com.bookstore.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 6/27/2017.
 */
@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;


    @Override
    public void create(Review review, User user, Product product, int rating, String comment, String time) {
        review.setRating(rating);
        review.setComment(comment);
        review.setUser(user);
        review.setProduct(product);
        review.setTime(time);
        reviewRepository.save(review);
    }

    @Override
    public List<Review> getAll() {
        return (List<Review>) reviewRepository.findAll();
    }

}
