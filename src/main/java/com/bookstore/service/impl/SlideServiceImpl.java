package com.bookstore.service.impl;

import com.bookstore.domain.Slide;
import com.bookstore.repository.SlideRepository;
import com.bookstore.service.SlideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 7/1/2017.
 */
@Service
public class SlideServiceImpl implements SlideService{
    @Autowired
    private SlideRepository slideRepository;

    @Override
    public List<Slide> getAll() {
        return (List<Slide>) slideRepository.findAll();

    }

    @Override
    public Slide fingOne(Long id) {
        return slideRepository.findOne(id);
    }
}
