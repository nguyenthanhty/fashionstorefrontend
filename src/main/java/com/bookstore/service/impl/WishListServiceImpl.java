package com.bookstore.service.impl;

import com.bookstore.domain.Product;
import com.bookstore.domain.User;
import com.bookstore.domain.WishList;
import com.bookstore.repository.WishListRepository;
import com.bookstore.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
@Service
public class WishListServiceImpl implements WishListService {
    @Autowired
    private WishListRepository wishListRepository;
    @Override
    public void create(WishList wishlist, User user, Product product) {
        wishlist.setUser(user);
        wishlist.setProduct(product);
        wishListRepository.save(wishlist);
    }
    @Override
    public List<WishList> getAll() {
        return (List<WishList>) wishListRepository.findAll();
    }


}
