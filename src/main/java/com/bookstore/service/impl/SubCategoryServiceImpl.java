package com.bookstore.service.impl;

import com.bookstore.domain.SubCategory;
import com.bookstore.repository.SubCategoryRepository;
import com.bookstore.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 7/2/2017.
 */
@Service
public class SubCategoryServiceImpl implements SubCategoryService {
    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Override
    public List<SubCategory> getAll() {
        return (List<SubCategory>) subCategoryRepository.findAll();
    }

    @Override
    public SubCategory findOne(Long id) {
        return subCategoryRepository.findOne(id);
    }

    @Override
    public List<SubCategory> findSubCategoriesByCategory_Id(Long id) {
        return subCategoryRepository.findSubCategoriesByCategory_Id(id);
    }
}
