package com.bookstore.service;

import com.bookstore.domain.Product;
import com.bookstore.domain.Review;
import com.bookstore.domain.User;

import java.util.List;

/**
 * Created by Administrator on 6/27/2017.
 */
public interface ReviewService {

    void create(Review review, User user, Product product, int rating, String comment, String time);
    List<Review> getAll();
}
