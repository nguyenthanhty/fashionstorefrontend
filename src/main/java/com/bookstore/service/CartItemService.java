package com.bookstore.service;

import com.bookstore.domain.CartItem;
import com.bookstore.domain.Product;
import com.bookstore.domain.ShoppingCart;
import com.bookstore.domain.User;

import java.util.List;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
public interface CartItemService {

    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);

    CartItem updateCartItem(CartItem cartItem);

    CartItem addProductToCartItem(Product product, String color, String size, User user, int qty);

    CartItem findById(Long id);

    void removeCartItem(CartItem byId);

    CartItem save(CartItem cartItem);
}
