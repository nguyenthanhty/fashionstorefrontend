package com.bookstore.service;

import com.bookstore.domain.UserPayment;

/**
 * Created by nguyenthanhty on 6/27/17.
 */
public interface UserPaymentService {
    UserPayment findById(Long id);

    void removeById(Long id);
}
