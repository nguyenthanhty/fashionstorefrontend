package com.bookstore.service;

import com.bookstore.domain.UserShipping;

/**
 * Created by nguyenthanhty on 6/27/17.
 */
public interface UserShippingService {
    UserShipping findById(Long id);

    void removeById(Long id);
}
