package com.bookstore.service;

import com.bookstore.domain.Category;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface CategoryService {

    List<Category> getAll();

    Category findById(Long id);
}
