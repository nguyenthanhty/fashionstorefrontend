package com.bookstore.service;

import com.bookstore.domain.Payment;
import com.bookstore.domain.UserPayment;

/**
 * Created by nguyenthanhty on 7/2/17.
 */
public interface PaymentService {
    Payment setByUserPayment(UserPayment userPayment, Payment payment);
}
