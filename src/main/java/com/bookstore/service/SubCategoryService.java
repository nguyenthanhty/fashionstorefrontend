package com.bookstore.service;

import com.bookstore.domain.SubCategory;

import java.util.List;

/**
 * Created by MinhToan on 7/2/2017.
 */
public interface SubCategoryService {

    List<SubCategory> getAll();

    SubCategory findOne(Long id);

    List<SubCategory> findSubCategoriesByCategory_Id(Long id);

}
