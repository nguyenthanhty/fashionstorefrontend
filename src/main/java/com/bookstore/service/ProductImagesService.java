package com.bookstore.service;

import com.bookstore.domain.ProductImages;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface ProductImagesService {

    List<ProductImages> getAll();

    ProductImages findById(Long id);
}
