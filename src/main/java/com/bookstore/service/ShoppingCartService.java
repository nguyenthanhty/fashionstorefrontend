package com.bookstore.service;

import com.bookstore.domain.ShoppingCart;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
public interface ShoppingCartService {

    ShoppingCart updateShoppingCart(ShoppingCart shoppingCart);

    void clearShoppingCart(ShoppingCart shoppingCart);
}
