package com.bookstore.service;

import com.bookstore.domain.Slide;

import java.util.List;

/**
 * Created by MinhToan on 7/1/2017.
 */
public interface SlideService {

    List<Slide> getAll();

    Slide fingOne(Long id);
}
