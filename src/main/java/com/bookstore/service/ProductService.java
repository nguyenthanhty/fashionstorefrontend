package com.bookstore.service;

import com.bookstore.domain.Product;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface ProductService {

    List<Product> getAll();

    Product findOne(Long id);

    List<Product> findByStatus(String status);

    List<Product> findByGender(String gender);

//    List<Product> findBySubCategoryId(Long id);

    List<Product> findByGenderAndStatus(String gender,String status);

    List<Product> findBySubCategory(String subCategory);

    List<Product> searchProduct(String keyword);
}
