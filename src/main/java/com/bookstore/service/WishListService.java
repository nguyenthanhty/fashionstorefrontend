package com.bookstore.service;

import com.bookstore.domain.Product;
import com.bookstore.domain.User;
import com.bookstore.domain.WishList;

import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
public interface WishListService {

    void create(WishList wishlist, User user, Product product);
    List<WishList> getAll();

}
