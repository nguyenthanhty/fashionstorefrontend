package com.bookstore.service;

import com.bookstore.domain.*;

/**
 * Created by nguyenthanhty on 7/3/17.
 */
public interface OrderService {

    Order createOrder(ShoppingCart shoppingCart,
                      ShippingAddress shippingAddress,
                      BillingAddress billingAddress,
                      Payment payment, User user);
    Order findOne(Long id);
}
