package com.bookstore.controller;

import com.bookstore.domain.Product;
import com.bookstore.domain.ProductImages;
import com.bookstore.domain.User;
import com.bookstore.domain.WishList;
import com.bookstore.service.ProductImagesService;
import com.bookstore.service.ProductService;
import com.bookstore.service.UserService;
import com.bookstore.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
@Controller
/*@RequestMapping(value = "/wishlist")*/
public class WishListController {
    @Autowired
    private WishListService wishListService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductImagesService productImagesService;

    @RequestMapping(value = "/addWishProduct",method = RequestMethod.GET)
    public String addWishList(Model model, @ModelAttribute("product") Product product, Principal principal){
        WishList wishList = new WishList();
        User user = userService.findByUsername(principal.getName());
        List<WishList> wishLists = wishListService.getAll();
        if (wishLists.size() == 0) {
            wishListService.create(wishList,user, product);
            model.addAttribute("wishlist", wishList);
            return "productDetail";
        } else {
            boolean flag =false;
            for (WishList item : wishLists){
                if (item.getUser().getId()==user.getId() && item.getProduct().getId()==product.getId()){
                    flag = true;
                    model.addAttribute("DoubleClick", true);
                    break;
                }
            }
            if (!flag){
                wishListService.create(wishList,user, product);
                model.addAttribute("wishlist", wishList);
            }

        }

        Product productItem = productService.findOne(product.getId());
        model.addAttribute("product", productItem);
        List<String> sizeList = Arrays.asList("S","M", "L", "XL");
        model.addAttribute("sizeList", sizeList);

        List<String> colorList = Arrays.asList("Black", "Blue", "Red","White");
        model.addAttribute("colorList", colorList);

        List<ProductImages> productImagesList = productImagesService.getAll();
        model.addAttribute("productImagesList",productImagesList);

        List<Product>listProduct = productService.findByStatus(productItem.getStatus());

        model.addAttribute("listProduct",listProduct);

        return "productDetail";
    }

    @RequestMapping(value = "/listWishList",method = RequestMethod.GET)
    public String listWishList(Model model,Principal principal){
        User user = userService.findByUsername(principal.getName());
        user.getId();
        List<WishList> listwishlist = wishListService.getAll();
        List<WishList> listWishlist = new ArrayList<>();
        for(WishList Items : listwishlist ){
            if (Items.getUser().getId()==user.getId()){
                listWishlist.add(Items);
            }

        }
        model.addAttribute("listWishlist",listWishlist);
        model.addAttribute("user", user);
        model.addAttribute("userPaymentList", user.getUserPaymentList());
        model.addAttribute("userShippingList", user.getUserShippingList());

        model.addAttribute("listOfCreditCards", true);
        model.addAttribute("classActiveBilling", true);
        model.addAttribute("listOfShippingAddresses", true);
        model.addAttribute("listOfProductWishlist", true);
        return "myProfile";
    }
}
