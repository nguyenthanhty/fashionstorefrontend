package com.bookstore.controller;

import com.bookstore.domain.User;
import com.bookstore.domain.UserShipping;
import com.bookstore.service.UserService;
import com.bookstore.utiliy.ExpiredMonths;
import com.bookstore.utiliy.VNConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.Collections;
import java.util.List;

/**
 * Created by nguyenthanhty on 7/5/17.
 */
@Controller
public class MyProfileController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/myprofile", method= RequestMethod.GET)
    public String myProfile(Principal principal, Model model){
        User user = userService.findByUsername(principal.getName());
        model.addAttribute("user", user);

        model.addAttribute("userPaymentList", user.getUserPaymentList());
        model.addAttribute("userShippingList", user.getUserShippingList());
//        model.addAttribute("orderList", user.getOrderList());

        UserShipping userShipping = new UserShipping();
        model.addAttribute("userShipping", userShipping);

//        model.addAttribute("listOfCreditCards", true);
//        model.addAttribute("listOfShippingAddresses", true);

        List<String> provinceList = VNConstants.listOfVNProvinceCode;
        Collections.sort(provinceList);
        model.addAttribute("provinceList", provinceList);
        model.addAttribute("showPaymentInformation", true);

        List<String> expiredMonthList = ExpiredMonths.listOfMonthCode;
        model.addAttribute("expiredMonthList", expiredMonthList);

        return "myProfile";

    }
}
