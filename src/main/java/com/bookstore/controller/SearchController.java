package com.bookstore.controller;

import com.bookstore.domain.Product;
import com.bookstore.domain.User;
import com.bookstore.service.ProductService;
import com.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

/**
 * Created by nguyenthanhty on 7/7/17.
 */
@Controller
public class SearchController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchByCategory(
            @RequestParam("keyword") String keyword,
            Model model, Principal principal
    ){
        if(principal!=null) {
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        List<Product> productList = productService.searchProduct(keyword);

        if (productList.isEmpty()) {
            model.addAttribute("emptyList", true);
            return "search-result";
        }

        model.addAttribute("productList", productList);

        return "search-result";
    }
}
