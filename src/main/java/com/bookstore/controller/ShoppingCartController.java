package com.bookstore.controller;

import com.bookstore.domain.CartItem;
import com.bookstore.domain.Product;
import com.bookstore.domain.ShoppingCart;
import com.bookstore.domain.User;
import com.bookstore.service.CartItemService;
import com.bookstore.service.ProductService;
import com.bookstore.service.ShoppingCartService;
import com.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
@Controller
//@RequestMapping(value = "/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private ShoppingCartService shoppingCartService;


    @RequestMapping(value = "/cart")
    public String shippingCart(Model model, Principal principal){
        User user = userService.findByUsername(principal.getName());
        ShoppingCart shoppingCart = user.getShoppingCart();

        List<CartItem> cartItemList =cartItemService.findByShoppingCart(shoppingCart);

        shoppingCartService.updateShoppingCart(shoppingCart);

        model.addAttribute("cartItemList", cartItemList);
        model.addAttribute("shoppingCart", shoppingCart);

        return "shopping-cart";
    }

    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public String addCart(
            @ModelAttribute("product") Product product,
            @ModelAttribute("qty") String qty,
            @ModelAttribute("color") String color,
            @ModelAttribute("size") String size,
            Model model,
            Principal principal
    ){
        User user = userService.findByUsername(principal.getName());
        product = productService.findOne(product.getId());

        if(Integer.parseInt(qty) > product.getInStockNumber()){
            model.addAttribute("notEnoughStock",true);
            return "forward:/productDetails?id=" + product.getId();

        }

        CartItem cartItem = cartItemService.addProductToCartItem(product, color, size, user, Integer.parseInt(qty));

        model.addAttribute("addBookSuccess",true);

        return "forward:/cart";

    }

    @RequestMapping(value="/updateCartItem", method=RequestMethod.POST)
    public String updateCartItem(
            @ModelAttribute("id") Long cartItemId,
            @ModelAttribute("qty") int qty
    ) {
        CartItem cartItem = cartItemService.findById(cartItemId);
        cartItem.setQty(qty);
        cartItemService.updateCartItem(cartItem);

        return "forward:/cart";
    }

    @RequestMapping("/removeItem")
    public String removeItem(@RequestParam("id") Long cartItemId){
        cartItemService.removeCartItem(cartItemService.findById(cartItemId));

        return "forward:/cart";
    }
}
