package com.bookstore.controller;

import com.bookstore.domain.Product;
import com.bookstore.domain.ProductImages;
import com.bookstore.service.ProductImagesService;
import com.bookstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nguyenthanhty on 7/1/17.
 */

@Controller
public class ProductDetailController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductImagesService productImagesService;

    @RequestMapping(value = "/productDetail")
    public String productDetails(@RequestParam("id") Long productId, Model model){

        Product product = productService.findOne(productId);
        model.addAttribute("product", product);

        List<String> sizeList = Arrays.asList("S","M", "L", "XL");
        model.addAttribute("sizeList",sizeList);

        List<String> colorList = Arrays.asList("Black", "Blue", "Red","White");
        model.addAttribute("colorList",colorList);

        String status = product.getStatus();

        List<Product>listProduct = productService.findByStatus(status);
        model.addAttribute("listProduct",listProduct);

        List<ProductImages> productImagesList = productImagesService.getAll();
        model.addAttribute("productImagesList",productImagesList);

        return "productDetail";
    }
}
