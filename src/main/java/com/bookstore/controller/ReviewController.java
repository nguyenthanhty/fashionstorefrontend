package com.bookstore.controller;

import com.bookstore.domain.Product;
import com.bookstore.domain.ProductImages;
import com.bookstore.domain.Review;
import com.bookstore.domain.User;
import com.bookstore.service.ProductImagesService;
import com.bookstore.service.ProductService;
import com.bookstore.service.ReviewService;
import com.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 6/27/2017.
 */
@Controller
/*@RequestMapping(value = "/review")*/
public class ReviewController {
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    @Autowired
    private ProductImagesService productImagesService;

    @RequestMapping(value = "/add")
    public String addReview(Model model){
        Review review = new Review();
        model.addAttribute("review", review);

        return "index";
    }
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private String saveReview(
            @ModelAttribute("product") Product product,
            @ModelAttribute("rating") int rating,
            @ModelAttribute("comment") String comment,
            @ModelAttribute("time") String time,
            Model model, Principal principal
    ) {
        Date today = new Date((System.currentTimeMillis()));
        SimpleDateFormat timeFormat= new SimpleDateFormat("hh:mm:ss dd/MM/yyyy");
        time =timeFormat.format(today.getTime());
        Review review = new Review();
        User user = userService.findByUsername(principal.getName());
        /*reviewService.create(review, user,rating, product, comment);*/
        reviewService.create(review,user,product,rating,comment,time);
        model.addAttribute("review", review);
        List<Review> reviewList = reviewService.getAll();
        model.addAttribute("reviewList",reviewList);

        Product productItem = productService.findOne(product.getId());
        model.addAttribute("product", productItem);
        List<String> sizeList = Arrays.asList("S","M", "L", "XL");
        model.addAttribute("sizeList", sizeList);

        List<String> colorList = Arrays.asList("Black", "Blue", "Red","White");
        model.addAttribute("colorList", colorList);

        List<ProductImages> productImagesList = productImagesService.getAll();
        model.addAttribute("productImagesList",productImagesList);

        return "productDetail";
    }
    @RequestMapping(value = "/reviewList",method = RequestMethod.GET)
    public String listReview(Model model, Principal principal){
        User user = userService.findByUsername(principal.getName());
        user.getId();
        List<Review> listreview = reviewService.getAll();
        List<Review> listReview = new ArrayList<>();
        for(Review Items : listreview ){
            if (Items.getUser().getId()== user.getId()){
                listReview.add(Items);
            }
        }
        model.addAttribute("user", user);
        model.addAttribute("userPaymentList", user.getUserPaymentList());
        model.addAttribute("userShippingList", user.getUserShippingList());
//        model.addAttribute("orderList", user.orderList());

        model.addAttribute("listOfCreditCards", true);
        model.addAttribute("classActiveBilling", true);
        model.addAttribute("listOfShippingAddresses", true);
      /*  model.addAttribute("showListOfShippingAddresses", true);*/
        model.addAttribute("listReview",listReview);
        model.addAttribute("listOfProductReview", true);
        return"myProfile";
    }
}
