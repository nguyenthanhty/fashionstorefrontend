package com.bookstore.controller;

import java.security.Principal;
import java.util.*;
import com.bookstore.domain.*;
import com.bookstore.domain.Slide;
import com.bookstore.service.*;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @Autowired
    private UserService userService;

    @Autowired
    private SlideService slideService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;


    @RequestMapping("/")
    public String index(Model model) {
        List<Product> listProduct = productService.getAll();
        model.addAttribute("listProduct",listProduct);

        String male = "Male";
        String female = "Female";

        model.addAttribute("male", male);
        model.addAttribute("female", female);

        List<Slide> slideList1 = slideService.getAll();
        model.addAttribute("slideList1",slideList1);

        return "index";
    }

    @RequestMapping(value = "/productcategory", method = RequestMethod.GET)
    public String productCategory(@RequestParam("gender") String gender, String status,  Model model) {

        List<Category> categoryList = categoryService.getAll();

        List<Product> hotProductList = new ArrayList<>();
        List<Product> newProductList = new ArrayList<>();


        List<Product> hotList = productService.findByStatus("Hot");
        for (Product productItem : hotList){
            if (productItem.getGender().equals(gender)) {
                hotProductList.add(productItem);
            }
        }
        model.addAttribute("hotProductList", hotProductList);

        List<Product> newList = productService.findByStatus("New");
        for (Product productItem : newList){
            if (productItem.getGender().equals(gender)) {
                newProductList.add(productItem);
            }
        }
        model.addAttribute("newProductList", newProductList);

        for (Category cateItem : categoryList) {
            String hrefId = "#" + cateItem.getId();
            cateItem.setHrefId(hrefId);
        }
        model.addAttribute("categoryList", categoryList);

        List<SubCategory> subCategoryList = subCategoryService.getAll();
        model.addAttribute("subCategoryList", subCategoryList);

        List<Product> productListGender = productService.findByGender(gender);
        model.addAttribute("productListGender",productListGender);
        model.addAttribute("gender", gender);

        if (gender.equals("Male")) {
            model.addAttribute("gioitinh", "Nam");
        } else {
            model.addAttribute("gioitinh", "Nữ");
        }

        return "categoryProduct";
    }
}
