package com.bookstore.controller;

import com.bookstore.domain.Product;
import com.bookstore.repository.CategoryRepository;
import com.bookstore.repository.ProductRepository;
import com.bookstore.repository.SubCategoryRepository;
import com.bookstore.service.ProductService;
import com.bookstore.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MinhToan on 7/5/2017.
 */
@Controller
public class CategoryProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private com.bookstore.service.SubCategoryService SubCategoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @RequestMapping("/getProductByCategory")
    public String getProductByCategory(
            @RequestParam("id") Long subCateId,
            @RequestParam("gender") String gender,
            Model model) {
        List<Product> productFiltered = new ArrayList<>();
        List<Product> hotProductList = new ArrayList<>();
        List<Product> newProductList = new ArrayList<>();

        List<Product> productListByGender = productService.findByGender(gender);
        for (Product productItem : productListByGender) {
            if (productItem.getSubCategory().getId() == subCateId) {
                productFiltered.add(productItem);
            }
        }

        List<Product> hotList = productService.findByStatus("Hot");
        for (Product productItem : hotList){
            if (productItem.getSubCategory().getId() == subCateId && productItem.getGender().equals(gender)) {
                hotProductList.add(productItem);
            }
        }
        model.addAttribute("hotProductList", hotProductList);

        List<Product> newList = productService.findByStatus("New");
        for (Product productItem : newList){
            if (productItem.getSubCategory().getId() == subCateId && productItem.getGender().equals(gender)) {
                newProductList.add(productItem);
            }
        }
        model.addAttribute("newProductList", newProductList);

        model.addAttribute("productListGender", productListByGender);
        model.addAttribute("productFilter", productFiltered);

        return "categoryProduct";
    }
}
