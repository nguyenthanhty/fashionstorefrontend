package com.bookstore.utiliy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nguyenthanhty on 7/4/17.
 */
public class ExpiredMonths {
    public final static String expiredMonth = "expiredMonth";

    public final static Map<String, String> mapOfExpiredMonth = new HashMap<String, String>() {
        {
            put("Tháng một", "Tháng một");
            put("Tháng hai", "Tháng hai");
            put("Tháng ba", "Tháng ba");
            put("Tháng tư", "Tháng tư");
            put("Tháng năm", "Tháng sáu");
            put("Tháng bảy", "Tháng tám");
            put("Tháng chín", "Tháng chín");
            put("Tháng mười", "Tháng mười");
            put("Tháng mười một", "Tháng mười một");
            put("Tháng mười hai", "Tháng mười hai");
        }
    };

    public final static List<String> listOfMonthCode = new ArrayList<>(mapOfExpiredMonth.keySet());
    public final static List<String> listOfMonthValue = new ArrayList<>(mapOfExpiredMonth.values());
}
