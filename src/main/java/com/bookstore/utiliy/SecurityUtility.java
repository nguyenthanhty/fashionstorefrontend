package com.bookstore.utiliy;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by MinhToan on 6/19/2017.
 */
@Component
public class SecurityUtility {
    private static  final  String SALT="salt";

    @Bean
    public static String ramdomPassword(){
        String SLATCHARS = "ABCEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random ramdom = new Random();

        while (salt.length()<18){
            int index = (int)(ramdom.nextFloat()*SLATCHARS.length());
            salt.append(SLATCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @Bean
    public static BCryptPasswordEncoder passwordEncoder(){
        return  new BCryptPasswordEncoder(12, new SecureRandom(SALT.getBytes()));

    }
}
