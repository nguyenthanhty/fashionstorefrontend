package com.bookstore.utiliy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nguyenthanhty on 6/25/17.
 */
public class VNConstants {
    public final static String VN = "VN";

    public final static Map<String, String> mapOfVNProvinces = new HashMap<String, String>() {
        {
            put("Hà nội", "Hà Nội");
            put("Đà nẳng", "Đà Nẳng");
            put("Thừa thiên huế ", "Thừa Thiên Huế");
            put("Hồ chí minh", "Hồ Chí Minh");
            put("Quảng nam", "Quảng Nam");
            put("Nha trang", "Nha Trang");
            put("Quảng trị", "Quảng Trị");
        }
    };

    public final static List<String> listOfVNProvinceCode = new ArrayList<>(mapOfVNProvinces.keySet());
    public final static List<String> listOfVNProvinceName = new ArrayList<>(mapOfVNProvinces.values());
}
