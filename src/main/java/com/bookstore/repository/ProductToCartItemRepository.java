package com.bookstore.repository;

import com.bookstore.domain.CartItem;
import com.bookstore.domain.ProductToCartItem;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
@Transactional
public interface ProductToCartItemRepository extends CrudRepository<ProductToCartItem,Long> {

    void deleteByCartItem(CartItem cartItem);
}
