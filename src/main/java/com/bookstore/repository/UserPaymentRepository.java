package com.bookstore.repository;

import com.bookstore.domain.UserPayment;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nguyenthanhty on 6/27/17.
 */
public interface UserPaymentRepository extends CrudRepository<UserPayment, Long> {

}
