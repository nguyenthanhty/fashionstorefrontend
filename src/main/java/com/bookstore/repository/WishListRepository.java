package com.bookstore.repository;

import com.bookstore.domain.User;
import com.bookstore.domain.WishList;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
public interface WishListRepository extends CrudRepository<WishList,Long > {
}
