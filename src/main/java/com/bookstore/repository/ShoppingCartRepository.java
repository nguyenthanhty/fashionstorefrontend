package com.bookstore.repository;

import com.bookstore.domain.ShoppingCart;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nguyenthanhty on 6/26/2017.
 */
public interface ShoppingCartRepository  extends CrudRepository<ShoppingCart,Long>{

}
