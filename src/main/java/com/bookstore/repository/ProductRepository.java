package com.bookstore.repository;

import com.bookstore.domain.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface ProductRepository extends CrudRepository<Product,Long> {
    List<Product> findByStatus(String status);


    @Query("select p from  Product p left  join  p.subCategory c where c.id = :id")
    List<Product> getListProductBySubCategoryId(@Param("id")Long id);

    List<Product> findByGender(String gender);

    List<Product> findBySubCategory(String subCategory);

    List<Product> findByGenderAndStatus(String gender,String status);

    List<Product> findByNameContaining(String name);
}
