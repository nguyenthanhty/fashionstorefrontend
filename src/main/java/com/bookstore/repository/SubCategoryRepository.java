package com.bookstore.repository;

import com.bookstore.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by MinhToan on 7/2/2017.
 */
public interface SubCategoryRepository extends CrudRepository<SubCategory,Long>{

    SubCategory findById(Long id);

    List<SubCategory> findSubCategoriesByCategory_Id(Long id);
}
