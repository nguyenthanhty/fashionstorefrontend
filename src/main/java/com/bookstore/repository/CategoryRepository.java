package com.bookstore.repository;

import com.bookstore.domain.Category;
import com.bookstore.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface CategoryRepository extends CrudRepository<Category,Long> {
    Category findBySubCategoryList(List<SubCategory> subCategoryList);

}
