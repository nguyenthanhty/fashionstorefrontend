package com.bookstore.repository;

import com.bookstore.domain.Order;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nguyenthanhty on 7/3/17.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
}
