package com.bookstore.repository;

import com.bookstore.domain.UserShipping;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by nguyenthanhty on 6/27/17.
 */
public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {
}
