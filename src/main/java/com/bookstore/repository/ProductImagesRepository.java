package com.bookstore.repository;

import com.bookstore.domain.Product;
import com.bookstore.domain.ProductImages;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by MinhToan on 6/24/2017.
 */
public interface ProductImagesRepository extends CrudRepository<ProductImages,Long> {
}
