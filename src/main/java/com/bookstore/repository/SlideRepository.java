package com.bookstore.repository;

import com.bookstore.domain.Slide;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by MinhToan on 7/1/2017.
 */
public interface SlideRepository extends CrudRepository<Slide,Long>{
}
