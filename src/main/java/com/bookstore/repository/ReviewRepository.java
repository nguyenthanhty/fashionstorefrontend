package com.bookstore.repository;

import com.bookstore.domain.Review;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Administrator on 6/27/2017.
 */
public interface ReviewRepository extends CrudRepository<Review,Long> {
    List<Review> findReviewsBy(Long productId);
}
