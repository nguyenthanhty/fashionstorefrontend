package com.bookstore.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by MinhToan on 6/21/2017.
 */
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String hrefId;

    public String getHrefId() {
        return hrefId;
    }

    public void setHrefId(String hrefId) {
        this.hrefId = hrefId;
    }

    @OneToMany(mappedBy = "category",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<SubCategory> subCategoryList;

    public List<SubCategory> getSubCategoryList() {
        return subCategoryList;
    }

    public void setSubCategoryList(List<SubCategory> subCategoryList) {
        this.subCategoryList = subCategoryList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Category(){

    }

    public Category(String name) {
        this.name = name;
    }
}
