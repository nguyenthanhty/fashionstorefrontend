package com.bookstore.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by MinhToan on 6/29/2017.
 */
@Entity
@Table
public class SubCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    private String name;

    @ManyToOne(targetEntity = Category.class)
    @JoinColumn(name = "subCategory_id",referencedColumnName = "id" )
    private Category category;

    @OneToMany(mappedBy = "subCategory",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    public List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public SubCategory(){

    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public SubCategory(String name, Category category, List<Product> productList) {
        this.name = name;
        this.category = category;
        this.productList = productList;
    }
}
