package com.bookstore.domain;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Entity
@Table(name = "productimages")
public class ProductImages {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String urlImage;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_id")
    private Product product;

    @Transient
    private MultipartFile productimages;

    public MultipartFile getProductimages() {
        return productimages;
    }

    public void setProductimages(MultipartFile productimages) {
        this.productimages = productimages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductImages(String urlImage, Product product) {
        this.urlImage = urlImage;
        this.product = product;
    }
    public  ProductImages(){

    }
}
