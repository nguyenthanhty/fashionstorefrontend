/**
 * Created by nguyenthanhty on 6/30/17.
 */

jQuery(document).ready(function($){

    $("#theSameAsShippingAddress").click(function(){
        if($("#theSameAsShippingAddress").is(":checked")) {
            $(".billingAddress").prop("disabled", true);
        } else {
            $(".billingAddress").prop("disabled", false);
        }
    });

    $("#txtConfirmPassword").blur(function(){
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if(password == "" && confirmPassword == "") {
            $("#checkPasswordMatch").html("");
            $("#updateUserInfoButton").prop('disabled', true);
        } else {
            if (password != confirmPassword) {
                $("#checkPasswordMatch").html("* Hai mật khẩu không trùng nhau.");
                $("#updateUserInfoButton").prop('disabled', true);
            } else {
                $("#updateUserInfoButton").prop('disabled', false);
            }
        }
    });

    $(".disableInputs").prop("disabled", true);
});
